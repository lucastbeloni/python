def computador_escolhe_jogada(n, m):
    computadorRemove = 1

    while computadorRemove != m:
        if (n - computadorRemove) % (m+1) == 0:
            return computadorRemove
        else:
            computadorRemove += 1
    return computadorRemove


def usuario_escolhe_jogada(n, m):
    jogadaPermitida = False

    while not jogadaPermitida:
        jogadorRemove = int(input('Quantas peças você vai tirar? '))
        if jogadorRemove > m or jogadorRemove < 1:
            print()
            print('Oops! Jogada inválida! Tente de novo.')
            print()

        else:
            jogadaPermitida = True

    return jogadorRemove


def campeonato():
    numeroRodada = 1
    while numeroRodada <= 3:
        
        print('\n**** Rodada', numeroRodada, '****\n')

        partida()
        numeroRodada += 1
        
    print("\nPlacar: Você 0 X 3 Computador")


def partida():
    n = int(input('Quantas peças? '))

    m = int(input('Limite de peças por jogada? '))

    vezDoPC = False

    if n % (m+1) == 0:
        print('\nVoce começa!')

    else:
        print('\nComputador começa!')
        vezDoPC = True

    while n > 0:
        if vezDoPC:
            computadorRemove = computador_escolhe_jogada(n, m)
            n = n - computadorRemove
            if computadorRemove == 1:
                print()
                print('O computador tirou uma peça')
            else:
                print()
                print('O computador tirou', computadorRemove, 'peças')

            vezDoPC = False
        else:
            jogadorRemove = usuario_escolhe_jogada(n, m)
            n = n - jogadorRemove
            if jogadorRemove == 1:
                print('\nVocê tirou uma peça')
            else:
                print('\nVocê tirou', jogadorRemove, 'peças')
            vezDoPC = True
        if n == 1:
            print('Agora resta apenas uma peça no tabuleiro.\n')
        else:
            if n != 0:
                print('Agora restam,', n, 'peças no tabuleiro.\n')

    print("Fim do jogo! O computador ganhou!")

print("Bem-vindo ao jogo do NIM! Escolha:\n")

print("1 - para jogar uma partida isolada")
print("2 - para jogar um campeonato\n")
tipoDePartida = int(input())

if tipoDePartida == 2:
    print('\nVoce escolheu um campeonato!\n')
    campeonato()
else:
    if tipoDePartida == 1:
        print()
        partida()
