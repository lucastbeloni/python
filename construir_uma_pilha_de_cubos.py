def find_nb(m):
    # your code
    volume = 0
    cubes = 0
    while volume < m:
        volume += (1 + cubes)**3
        cubes += 1
    if volume == m:
        return cubes
    else:
        return -1
