def count_positives_sum_negatives(arr):
    #your code here
    if not arr:
        return arr
    neg = 0
    pos = 0
    i = 0
    result = []
    for idx, item in enumerate(arr):

        if arr[idx] < 0:
            neg += arr[idx]

        elif arr[idx] > 0:
            pos += 1

    i += 1

    result.append(pos)
    result.append(neg)

    return result


def count_positives_sum_negatives_v02(arr):
    if not arr: return []
    pos = 0
    neg = 0
    for x in arr:
      if x > 0:
          pos += 1
      if x < 0:
          neg += x
    return [pos, neg]

def count_positives_sum_negatives_pro(arr):
    return [len([x for x in arr if x > 0])] + [sum(y for y in arr if y < 0)] if arr else []
