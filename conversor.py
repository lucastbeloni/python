def mask(cc):
    cc = list(cc)
    i = 0

    while i < len(cc)-4:
        cc[i] = '#'
        i += 1

    cc = ''.join(cc)
    print(cc)
    return cc

def mask_pro(cc):
    return '#' * (len(cc)-4) + cc[-4:]

mask(input("> "))

cc1 = mask_pro(input(">> "))
print(cc1)
