def nb_year(p0, percent, aug, p):
    i = 1
    while (p0 + p0*percent/100 + aug) < p:
        p0 = p0 + p0*percent/100 + aug
        i += 1
    return i

def nb_year_pro(population, percent, aug, target):
    year = 0
    while population < target:
        population += population * percent / 100. + aug
        year += 1
    return year
