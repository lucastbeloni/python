def filter_list(l):
    result = []
    for x in l:

        if type(x) is int:
            result.append(x)

    return result

def filter_list_pro(l):
  'return a new list with the strings filtered out'
  return [i for i in l if not isinstance(i, str)]
