def fizzbuzz(numero):
    if numero % 5 == 0 and numero % 3 == 0:
        return"FizzBuzz"
    elif numero % 5 == 0:
        return "Buzz"
    elif numero % 3 == 0:
        return "Fizz"
    else: return numero
