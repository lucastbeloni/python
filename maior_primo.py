def éPrimo(k):
    
    resultado = 0
    
    for div in range(1, k + 1):
        #print(div)
        if k % div == 0:
            resultado += 1
    if resultado == 2 and k != 1:
        return True
    else: return False   

def maior_primo(numero):

    maior = 0

    for num in range(1, numero + 1):

        if éPrimo(num):

            maior = num
    return maior

    
