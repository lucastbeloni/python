def productFib(prod):
    # your code
    atual = 1
    anterior = 0
    result = []
    while 1:

        Fib = anterior + atual
        anterior = atual
        atual = Fib

        if (anterior * atual) == prod:
            result.append(anterior)
            result.append(atual)
            result.append(True)

            return result
        elif (anterior * atual) >= prod:
            result.append(anterior)
            result.append(atual)
            result.append(False)
            return result

def productFib_pro(prod):
  a, b = 0, 1
  while prod > a * b:
    a, b = b, a + b
  return [a, b, prod == a * b]
