def is_perfect_square(n):
    x = n // 2
    y = set([x])
    print(f"y é {y}")
    while x * x != n:
        x = (x + (n // x)) // 2
        print(x)
        if x in y: return False
        y.add(x)
        print(f"y é {y}")
    return True

def find_next_square(sq):
    y = sq ** (0.5)

    if y % 1 == 0:
        sq = sq ** (0.5) + 1

        return sq ** 2

    else:
        return -1

def find_next_square_pro(sq):
    root = sq ** 0.5
    if root.is_integer():
        return (root + 1)**2
    return -1

#print(is_perfect_square(int(input("> "))))

print(find_next_square_pro(int(input("> "))))
