def is_square(n):
    root = n ** 0.5
    if n >= 0:
        if root.is_integer():
            return True
        else:
            return False
    return False

import math
def is_square_pro(n):
    return n > -1 and math.sqrt(n) % 1 == 0;

print(is_square(int(input("> "))))
