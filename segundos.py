seg = int(input("Por favor, entre com o número de segundos que deseja converter: "))

dias = str(seg // 86400)
seg = seg % 86400

horas = str(seg // 3600)
seg = seg % 3600

minutos = str(seg // 60)
seg = str(seg % 60)

print(dias, "dias,", horas, "horas,", minutos, "minutos e", seg, "segundos.")
