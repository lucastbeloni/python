numero = int(input("Digite um número: "))

resultado = 0

while numero != 0:
    resto = numero % 10
    numero = numero // 10
    resultado += resto

print(resultado)
