def series_sum(n):
    # Happy Coding ^_^
    i = 0
    result = 0
    while i < n:
        result += 1/(1 + i*3)
        i += 1
    return "{:.2f}".format(result)

def series_sum_pro(n):
    return '{:.2f}'.format(sum(1.0/(3 * i + 1) for i in range(n)))

print(series_sum(int(input("> "))))
